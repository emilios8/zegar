﻿/*
 * Utworzone przez SharpDevelop.
 * Użytkownik: Emilios
 * Data: 2014-10-21
 * Godzina: 11:39
 * 
 * Do zmiany tego szablonu użyj Narzędzia | Opcje | Kodowanie | Edycja Nagłówków Standardowych.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Zegar
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class Forma1 : Form
	{
		public Forma1()
		{
			//SetDisplayRectLocation(, );
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			
			
		}
		
		void Timer1Tick(object sender, EventArgs e)
		{
			labelGodzina.Text = DateTime.Now.ToLongTimeString();
		}
		
		void MainFormLoad(object sender, EventArgs e)
		{
			Location = new Point(Screen.PrimaryScreen.Bounds.Width - 270, 20);
			labelDzientygodnia.Text = DateTime.Now.ToLongDateString();
			labelGodzina.Text = DateTime.Now.ToLongTimeString();
		}
		
		
		void MainFormClick(object sender, EventArgs e)
		{
			DialogResult dialogResult = MessageBox.Show("Czy na pewno wyłączyć taki zajebisty zegar?", "Chcesz wyłączyć?", MessageBoxButtons.YesNo);
			if(dialogResult == DialogResult.Yes)
			{
    			Application.Exit();
			}
			else if (dialogResult == DialogResult.No)
			{
  		  		//do something else
			}
			
		}
		
		void LabelDzientygodniaClick(object sender, EventArgs e)
		{
			DialogResult dialogResult = MessageBox.Show("Czy na pewno wyłączyć taki zajebisty zegar?", "Chcesz wyłączyć?", MessageBoxButtons.YesNo);
			if(dialogResult == DialogResult.Yes)
			{
    			Application.Exit();
			}
			else if (dialogResult == DialogResult.No)
			{
  		  		//do something else
			}
		}
		
		void LabelGodzinaClick(object sender, EventArgs e)
		{
			DialogResult dialogResult = MessageBox.Show("Czy na pewno wyłączyć taki zajebisty zegar?", "Chcesz wyłączyć?", MessageBoxButtons.YesNo);
			if(dialogResult == DialogResult.Yes)
			{
    			Application.Exit();
			}
			else if (dialogResult == DialogResult.No)
			{
  		  		//do something else
			}
		}
	}
}
