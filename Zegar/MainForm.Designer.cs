﻿/*
 * Utworzone przez SharpDevelop.
 * Użytkownik: Emilios
 * Data: 2014-10-21
 * Godzina: 11:39
 * 
 * Do zmiany tego szablonu użyj Narzędzia | Opcje | Kodowanie | Edycja Nagłówków Standardowych.
 */
namespace Zegar
{
	partial class Forma1
	{
		/// <summary>
		/// Designer variable used to keep track of non-visual components.
		/// </summary>
		private System.ComponentModel.IContainer components = null;
		
		/// <summary>
		/// Disposes resources used by the form.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing) {
				if (components != null) {
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}
		
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.labelDzientygodnia = new System.Windows.Forms.Label();
			this.timer1 = new System.Windows.Forms.Timer(this.components);
			this.labelGodzina = new System.Windows.Forms.Label();
			this.SuspendLayout();
			// 
			// labelDzientygodnia
			// 
			this.labelDzientygodnia.Font = new System.Drawing.Font("Calibri", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.labelDzientygodnia.Location = new System.Drawing.Point(12, 9);
			this.labelDzientygodnia.Name = "labelDzientygodnia";
			this.labelDzientygodnia.Size = new System.Drawing.Size(228, 33);
			this.labelDzientygodnia.TabIndex = 0;
			this.labelDzientygodnia.Text = "Dzień tygodnia";
			this.labelDzientygodnia.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.labelDzientygodnia.Click += new System.EventHandler(this.LabelDzientygodniaClick);
			// 
			// timer1
			// 
			this.timer1.Enabled = true;
			this.timer1.Interval = 1000;
			this.timer1.Tick += new System.EventHandler(this.Timer1Tick);
			// 
			// labelGodzina
			// 
			this.labelGodzina.Font = new System.Drawing.Font("Calibri", 18F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.labelGodzina.Location = new System.Drawing.Point(12, 42);
			this.labelGodzina.Name = "labelGodzina";
			this.labelGodzina.Size = new System.Drawing.Size(228, 33);
			this.labelGodzina.TabIndex = 1;
			this.labelGodzina.Text = "Godzina";
			this.labelGodzina.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.labelGodzina.Click += new System.EventHandler(this.LabelGodzinaClick);
			// 
			// Forma1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(248, 77);
			this.ControlBox = false;
			this.Controls.Add(this.labelGodzina);
			this.Controls.Add(this.labelDzientygodnia);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "Forma1";
			this.Opacity = 0.75D;
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.Text = "Zegar";
			this.Load += new System.EventHandler(this.MainFormLoad);
			this.Click += new System.EventHandler(this.MainFormClick);
			this.ResumeLayout(false);
		}
		private System.Windows.Forms.Label labelGodzina;
		private System.Windows.Forms.Timer timer1;
		private System.Windows.Forms.Label labelDzientygodnia;
	}
}
